package com.akdenizA.badefy.Controller;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Akdeniz on 13/10/2016.
 */

public class ConnectionManager {

    public boolean checkInternetconnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInf = cm.getActiveNetworkInfo();
        if(netInf != null){
            return  netInf.isConnectedOrConnecting();
        }
        return  false;

    }
}
