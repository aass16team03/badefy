package com.akdenizA.badefy.Controller;

import android.content.Context;
import android.content.SharedPreferences;

import com.akdenizA.badefy.Model.BathingSpot;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Akdeniz on 13/10/2016.
 */

public class FileManager {

    public Boolean saveDataIntoFile(Context context, List<BathingSpot> spotList) {
        SharedPreferences sharedPref = context.getSharedPreferences("appData", Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefEditor = context.getSharedPreferences("appData", Context.MODE_WORLD_WRITEABLE).edit();
        Gson gson = new Gson();
        String json = gson.toJson(spotList);

        prefEditor.putString("jsondata", json);
        prefEditor.commit();
        return true;
    }

    public List<BathingSpot> loadDataFromFile(Context context) {

        Gson gson = new Gson();
        SharedPreferences sharedPref = context.getSharedPreferences("appData", Context.MODE_WORLD_WRITEABLE);
        String strJson = sharedPref.getString("jsondata", "0");

        BathingSpot[] spotArray = gson.fromJson(strJson, BathingSpot[].class);
        List<BathingSpot> spotList =  Arrays.asList(spotArray);

        return spotList;
    }

}
