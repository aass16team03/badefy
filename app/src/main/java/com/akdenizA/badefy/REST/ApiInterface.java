package com.akdenizA.badefy.REST;

/**
 * Created by Akdeniz on 10/10/2016.
 */

//TODO: organize your imports -> check if Android studio can do it automatically on save
import com.akdenizA.badefy.Model.BathingSpot;
import com.akdenizA.badefy.Model.BathingSpotsResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;



public interface ApiInterface {
    @GET("/lageso/gesundheit/gesundheitsschutz/badegewaesser/liste-der-badestellen/index.php/index/all.json?q=")
                                                 //@Body BathingSpot bathingSpot
    Call<BathingSpotsResponse> getBathingSpotList();

}