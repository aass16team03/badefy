package com.akdenizA.badefy.REST;

import com.akdenizA.badefy.Model.BathingSpot;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Akdeniz on 10/10/2016.
 */


public class ApiClient {

    public static final String BASE_URL = "http://www.berlin.de";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}