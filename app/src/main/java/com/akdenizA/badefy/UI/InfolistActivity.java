package com.akdenizA.badefy.UI;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.akdenizA.badefy.Controller.ConnectionManager;
import com.akdenizA.badefy.Controller.FileManager;
import com.akdenizA.badefy.Model.BathingSpot;
import com.akdenizA.badefy.Model.BathingSpotsResponse;
import com.akdenizA.badefy.R;
import com.akdenizA.badefy.REST.ApiClient;
import com.akdenizA.badefy.REST.ApiInterface;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class InfolistActivity extends AppCompatActivity {
    ListView infoListView;
    List<BathingSpot> bathingspotList = null;
    final FileManager filemanager = new FileManager();
    ConnectionManager connectionManager = new ConnectionManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //ProgressDialog.show(this, "Loading", "Wait while loading...");
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.title_infoList));
        setContentView(R.layout.activity_infolist);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        infoListView = (ListView) findViewById(R.id.infoListView);
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

        if (connectionManager.checkInternetconnection(InfolistActivity.this)) {
           ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<BathingSpotsResponse> call = apiService.getBathingSpotList();
            call.enqueue(new Callback<BathingSpotsResponse>() {
                @Override
                public void onResponse(Call<BathingSpotsResponse> call, Response<BathingSpotsResponse> response) {
                    List<BathingSpot> bslist = response.body().getResults();
                    Log.d("Retrofit onsuccess", "size of bathing spots" + bslist.size());
                    filemanager.saveDataIntoFile(InfolistActivity.this, bslist);
                    listBathingSpots(bslist);
                }

                @Override
                public void onFailure(Call<BathingSpotsResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("Retofit failure", t.toString());
                }

            });
        } else {
            bathingspotList = filemanager.loadDataFromFile(InfolistActivity.this);
            if (bathingspotList != null) {
                listBathingSpots(bathingspotList);
            } else {
                Log.d("InfolistActivity:", "Badelist is empty");
            }
        }

    }

    public void listBathingSpots(List<BathingSpot> s) {
        final List<BathingSpot> bade = s;
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

        HashMap<String, String> item;
        for (int i = 0; i < s.size(); i++) {
            item = new HashMap<String, String>();
            item.put("line1", s.get(i).getName());
            item.put("line2", s.get(i).getDistrict());
            list.add(item);
        }

        SimpleAdapter adapter = new SimpleAdapter(InfolistActivity.this, list,
                android.R.layout.two_line_list_item,
                new String[]{"line1", "line2"},
                new int[]{android.R.id.text1, android.R.id.text2});

        infoListView.setAdapter(adapter);

        infoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(InfolistActivity.this, InfoTabelleActivity.class);
                intent.putExtra(InfoTabelleActivity.BATHING_DATA_EXTRA ,bade.get(position));
                startActivity(intent);

            }
        });
    }



}
