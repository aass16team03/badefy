package com.akdenizA.badefy.UI;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.akdenizA.badefy.Controller.ConnectionManager;
import com.akdenizA.badefy.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    //TODO: Save cordinates in sharedPreferences to be able to call maps in offline-mode too

    private GoogleMap mMap;
    final float zoomLevel = 14.0f;
    ConnectionManager connectionManager = new ConnectionManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        EditText edit = (EditText) findViewById(R.id.searchEditText);
        Button but = (Button) findViewById(R.id.searchButton);
        edit.setVisibility(View.INVISIBLE);
        but.setVisibility(View.INVISIBLE);

        Log.d("Debug: OnCreate", "--------------------executing");
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        Bundle extras = getIntent().getExtras();

        if (extras != null && connectionManager.checkInternetconnection(MapsActivity.this)) {
            Log.d("OnMapReady: ", "--------------------extras not null");
            String rss = extras.getString("rss");
            searchAdress(rss, false);
        }else{
            Toast.makeText(MapsActivity.this, "You need to be connected to the internet!", Toast.LENGTH_LONG).show();
        }
    }

    public void onSearch(View view) {
        EditText searchLocation = (EditText) findViewById(R.id.searchEditText);
        String location = searchLocation.getText().toString();
        if (location != null || location.equals("")) {
            searchAdress(location, true);
        }

    }

    public void searchAdress(String s, Boolean regularSearch) {
        Log.d("Searching for", s);
        List<Address> adressList = null;
        if (s != null || s.equals("")) {
            Geocoder geocoder = new Geocoder(this);
            try {
                adressList = geocoder.getFromLocationName(s, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!adressList.isEmpty()) {
            Address address = adressList.get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            mMap.clear();
            mMap.addMarker(new MarkerOptions().position(latLng).title(s));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        } else {
            if (regularSearch) {
                Toast.makeText(this, "Adresse nicht gefunden", Toast.LENGTH_LONG).show();
                return;
            } else {
                if (!s.contains("Berlin")) {
                    String newSearchString = s;
                    if (newSearchString.contains("/")) {
                        newSearchString = newSearchString.substring(0, s.indexOf("/"));
                    }
                    if (newSearchString.contains("(")) {
                        newSearchString = newSearchString.substring(0, s.indexOf("("));
                    } else if (newSearchString.contains(",")) {
                        newSearchString = newSearchString.substring(0, s.indexOf(","));
                    }
                    if (newSearchString.contains("Seebadeanstalt")) {
                        newSearchString = newSearchString.replace("Seebadeanstalt", "");
                    }

                    newSearchString = newSearchString + " Berlin";

                    if (newSearchString.contains("Flussbad")) {
                        newSearchString = newSearchString.replace("Flussbad", "Bad");
                        newSearchString = newSearchString.replace("Berlin", "");
                    }
                    searchAdress(newSearchString, false);
                } else {
                    Toast.makeText(this, "Adresse nicht gefunden", Toast.LENGTH_LONG).show();
                    return;
                }
            }

        }

    }

}
