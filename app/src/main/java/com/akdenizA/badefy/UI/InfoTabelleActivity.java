package com.akdenizA.badefy.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.akdenizA.badefy.Model.BathingSpot;
import com.akdenizA.badefy.R;


public class InfoTabelleActivity extends AppCompatActivity {

    public static final String BATHING_DATA_EXTRA = "bathingData";

    String rssName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_tabelle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //TODO: check out butterknife
        ImageView qualiWertImageView = (ImageView) findViewById(R.id.qualiValueImageView);
        TextView badenameZeileTextView = (TextView) findViewById(R.id.bathingnameLineTextView);
        TextView badenameWertTextView = (TextView) findViewById(R.id.bathingnameValueTextView);
        TextView bezirkWertTextView = (TextView) findViewById(R.id.districtValueTextView);
        TextView datumWertTextView = (TextView) findViewById(R.id.dateValueTextView);
        TextView cbWertTextView = (TextView) findViewById(R.id.cbValueTextView);
        TextView ecoliWertTextView = (TextView) findViewById(R.id.ecoliValueTextView);
        TextView ieWertTextView = (TextView) findViewById(R.id.ieValueTextView);
        TextView sichtWertTextView = (TextView) findViewById(R.id.viewValueTextView);
        TextView tempWertTextView = (TextView) findViewById(R.id.tempValueTextView);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            //TODO: make this String static
            BathingSpot bs = (BathingSpot) extras.getParcelable(BATHING_DATA_EXTRA);


            setTitle(bs.getName() + " info table");

            badenameZeileTextView.setText("BathingSpot: ");
            badenameWertTextView.setText(bs.getName().replace(",", ",\n"));
            bezirkWertTextView.setText(bs.getDistrict());
            datumWertTextView.setText("\n" + bs.getDate());
            cbWertTextView.setText(bs.getCb());
            ecoliWertTextView.setText(bs.getEcoli());
            ieWertTextView.setText(bs.getIe());
            sichtWertTextView.setText(bs.getViewdepth());
            tempWertTextView.setText(bs.getTemperatur() + "\n");

            if (bs.getWaterquality() == 1) {
                qualiWertImageView.setImageResource(R.drawable.gruen);

            } else {
                qualiWertImageView.setImageResource(R.drawable.orange);

            }
        }

    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.zuruckButton:
                Intent backIntent = new Intent(InfoTabelleActivity.this, InfolistActivity.class);
                startActivity(backIntent);
                break;
            case R.id.mapButton:
                Intent mapIntent = new Intent(InfoTabelleActivity.this, MapsActivity.class);
                mapIntent.putExtra("rss", rssName);
                startActivity(mapIntent);
                break;
            default:
                break;
        }
    }


}
