package com.akdenizA.badefy.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Akdeniz on 10/10/2016.
 */

public class BathingSpotsResponse {

    @Getter @Setter
    @SerializedName("index")
    private List<BathingSpot> results;

    @Getter @Setter
    private int totalResults;

}
