package com.akdenizA.badefy.Model;


import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Setter;
import lombok.Getter;


public class BathingSpot implements Parcelable{

    @Getter @Setter
    @SerializedName("id")
    @Expose
    private int id;

    @Getter @Setter
    @SerializedName("wasserqualitaet")
    @Expose
    private int waterquality;

    @Getter @Setter
    @SerializedName("badname")
    @Expose
    private String name;

    @Getter @Setter
    @SerializedName("bezirk")
    @Expose
    private String district;

    @Getter @Setter
    @SerializedName("dat")
    @Expose
    //TODO: this should be a java.util.Date
    private String date;

    @Getter @Setter
    @SerializedName("cb")
    @Expose
    private String cb;

    @Getter @Setter
    @SerializedName("eco")
    @Expose
    private String ecoli;

    @Getter @Setter
    @SerializedName("ente")
    @Expose
    private String ie;

    @Getter @Setter
    @SerializedName("sicht")
    @Expose
    private String viewdepth;

    @Getter @Setter
    @SerializedName("temp")
    @Expose
    private String temperatur;

    @Getter @Setter
    @SerializedName("rss_name")
    @Expose
    private String rssName;


    public BathingSpot(int id, int  waterquality, String name, String date, String district, String cb,
                   String ecoli, String ie, String viewdepth, String temperatur, String rssName){
        this.id = id;
        this.waterquality = waterquality;
        this.name = name;
        this.date = date;
        this.district = district;
        this.cb = cb;
        this.ecoli = ecoli;
        this.ie = ie;
        this.viewdepth = viewdepth;
        this.temperatur = temperatur;
        this.rssName = rssName;

    }


    protected BathingSpot(Parcel in) {
        id = in.readInt();
        name = in.readString();
        district = in.readString();
        date = in.readString();
        cb = in.readString();
        ecoli = in.readString();
        ie = in.readString();
        viewdepth = in.readString();
        temperatur = in.readString();
        rssName = in.readString();
        waterquality = in.readInt();
    }


    //TODO: move statics to the top of file
    public static final Creator<BathingSpot> CREATOR = new Creator<BathingSpot>() {
        @Override
        public BathingSpot createFromParcel(Parcel in) {
            return new BathingSpot(in);
        }

    @Override
    public BathingSpot[] newArray(int size) {
        return new BathingSpot[size];
    }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(district);
        parcel.writeString(date);
        parcel.writeString(cb);
        parcel.writeString(ecoli);
        parcel.writeString(ie);
        parcel.writeString(viewdepth);
        parcel.writeString(temperatur);
        parcel.writeString(rssName);
        parcel.writeInt(waterquality);
    }
}
