package com.akdenizA.badefy.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Akdeniz on 13/10/2016.
 */

public class BathingSpot2 implements Parcelable {

    @Getter
    @Setter
    @SerializedName("id")
    @Expose
    private int id;

    @Getter @Setter
    @SerializedName("wasserqualitaet")
    @Expose
    private int waterquality;

    @Getter @Setter
    @SerializedName("badname")
    @Expose
    private String name;

    @Getter @Setter
    @SerializedName("bezirk")
    @Expose
    private String district;

    @Getter @Setter
    @SerializedName("dat")
    @Expose
    private String date;

    @Getter @Setter
    @SerializedName("cb")
    @Expose
    private String cb;

    @Getter @Setter
    @SerializedName("eco")
    @Expose
    private String ecoli;

    @Getter @Setter
    @SerializedName("ente")
    @Expose
    private String ie;

    @Getter @Setter
    @SerializedName("sicht")
    @Expose
    private String viewdepth;

    @Getter @Setter
    @SerializedName("temp")
    @Expose
    private String temperatur;

    @Getter @Setter
    @SerializedName("rss_name")
    @Expose
    private String rssName;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.waterquality);
        dest.writeString(this.name);
        dest.writeString(this.district);
        dest.writeString(this.date);
        dest.writeString(this.cb);
        dest.writeString(this.ecoli);
        dest.writeString(this.ie);
        dest.writeString(this.viewdepth);
        dest.writeString(this.temperatur);
        dest.writeString(this.rssName);
    }

    public BathingSpot2() {
    }

    protected BathingSpot2(Parcel in) {
        this.id = in.readInt();
        this.waterquality = in.readInt();
        this.name = in.readString();
        this.district = in.readString();
        this.date = in.readString();
        this.cb = in.readString();
        this.ecoli = in.readString();
        this.ie = in.readString();
        this.viewdepth = in.readString();
        this.temperatur = in.readString();
        this.rssName = in.readString();
    }

    public static final Parcelable.Creator<BathingSpot2> CREATOR = new Parcelable.Creator<BathingSpot2>() {
        @Override
        public BathingSpot2 createFromParcel(Parcel source) {
            return new BathingSpot2(source);
        }

        @Override
        public BathingSpot2[] newArray(int size) {
            return new BathingSpot2[size];
        }
    };
}
