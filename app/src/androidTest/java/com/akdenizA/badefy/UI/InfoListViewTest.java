package com.akdenizA.badefy.UI;


import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.MediumTest;
import com.akdenizA.badefy.R;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.anything;

/**
 * Created by Akdeniz on 14/10/2016.
 */

@RunWith(AndroidJUnit4.class)
@MediumTest
public class InfoListViewTest {

    @Rule
    public ActivityTestRule<InfolistActivity> mActivityRule = new ActivityTestRule<>(
            InfolistActivity.class);


    @Test
    public void listViewTest() throws InterruptedException {

        //TODO: Use a better solution to wait for network requests
        Thread.sleep(2000);
        onData(anything()).inAdapterView(withId(R.id.infoListView)).atPosition(0).perform(click());

    }


}
