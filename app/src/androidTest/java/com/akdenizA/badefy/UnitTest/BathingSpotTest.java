package com.akdenizA.badefy.UnitTest;


import android.os.Bundle;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;
import com.akdenizA.badefy.Model.BathingSpot;
import org.junit.Test;
import org.junit.runner.RunWith;
import static junit.framework.Assert.assertEquals;


@RunWith(AndroidJUnit4.class)
@SmallTest
public class BathingSpotTest {

    @Test
    public void IsBathingSpotParcelable(){

        BathingSpot bs = new BathingSpot(1, 1, "testSpot", "2016-10-12", "Steglitz", "<300", "<15", "<15", "250", "22,2", "Test Hof");
        Bundle bundle = new Bundle();
        bundle.putParcelable("spot", bs);
        BathingSpot parceledSpot = bundle.getParcelable("spot");

        assertEquals(bs, parceledSpot);

        //TODO: the code above does not test
        //TODO: test if it still works if the object only contians null values
        //TODO: negative test

//        BathingSpot bs = new BathingSpot(1, 1, "testSpot", "2016-10-12", "Steglitz", "<300", "<15", "<15", "250", "22,2", "Test Hof");
//        Parcel parcel = Parcel.obtain();
//        bs.writeToParcel(parcel, 0);
//        parcel.setDataPosition(0);
//
//        BathingSpot bsFromParcel = BathingSpot.CREATOR.createFromParcel(parcel);
//
//        assertEquals(bs, bsFromParcel);
//        assertThat(bsFromParcel.getName(), is("name"));


    }


}
