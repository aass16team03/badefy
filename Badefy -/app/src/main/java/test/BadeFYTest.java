package test;


import com.example.crazy.badefy.Badestelle;
import org.junit.Test;
import static junit.framework.Assert.assertEquals;

public class BadeFYTest {


    @Test
    public void testeSetterUndGetter(){
        Badestelle bade = new Badestelle();
        bade.setName("test");
        assertEquals(bade.getName(), "test");
    }
}
