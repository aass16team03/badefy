package com.example.crazy.badefy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class InfoTabelleActivity extends AppCompatActivity {

    String rssName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_tabelle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ImageView qualiWertImageView = (ImageView) findViewById(R.id.qualiWertImageView);
        TextView badenameZeileTextView = (TextView) findViewById(R.id.badenameZeileTextView);
        TextView badenameWertTextView = (TextView) findViewById(R.id.badenameWertTextView);
        TextView bezirkWertTextView = (TextView) findViewById(R.id.bezirkWertTextView);
        TextView datumWertTextView = (TextView) findViewById(R.id.datumWertTextView);
        TextView cbWertTextView = (TextView) findViewById(R.id.cbWertTextView);
        TextView ecoliWertTextView = (TextView) findViewById(R.id.ecoliWertTextView);
        TextView ieWertTextView = (TextView) findViewById(R.id.ieWertTextView);
        TextView sichtWertTextView = (TextView) findViewById(R.id.sichtWertTextView);
        TextView tempWertTextView = (TextView) findViewById(R.id.tempWertTextView);

        Button zuruckButton = (Button) findViewById(R.id.zuruckButton);
        Button mapButton = (Button) findViewById(R.id.mapButton);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String badename = extras.getString("badename");
            String bezirk = extras.getString("bezirk");
            String datum = extras.getString("datum");
            String cb = extras.getString("cb");
            String ecoli = extras.getString("ecoli");
            String ie = extras.getString("ie");
            String sicht = extras.getString("sicht");
            String temp = extras.getString("temp");
            int wasserquali = extras.getInt("quali");
            rssName = extras.getString("rss");

            setTitle(badename + " Infotabelle");

            badenameZeileTextView.setText("Badestelle: ");
            badenameWertTextView.setText(badename.replace(",", ",\n"));
            bezirkWertTextView.setText(bezirk);
            datumWertTextView.setText("\n" + datum);
            cbWertTextView.setText(cb);
            ecoliWertTextView.setText(ecoli);
            ieWertTextView.setText(ie);
            sichtWertTextView.setText(sicht);
            tempWertTextView.setText(temp + "\n");


            if (wasserquali == 1) {
                qualiWertImageView.setImageResource(R.drawable.gruen);

            } else {
                qualiWertImageView.setImageResource(R.drawable.orange);

            }
        }

    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.zuruckButton:
                Intent backIntent = new Intent(InfoTabelleActivity.this, InfolistActivity.class);
                startActivity(backIntent);
                break;
            case R.id.mapButton:
                Intent mapIntent = new Intent(InfoTabelleActivity.this, MapsActivity.class);
                mapIntent.putExtra("rss", rssName);
                startActivity(mapIntent);
                break;
            default:
                break;
        }
    }


}
