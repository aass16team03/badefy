package com.example.crazy.badefy;


public class Badestelle {
    private int id;
    private String name;
    private String bezirk;
    private String datum;
    private String cb;
    private String ecoli;
    private String ie;
    private String sichttiefe;
    private String temperatur;
    private String rssName;
    private int wasserquali;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBezirk() {
        return bezirk;
    }

    public void setBezirk(String bezirk) {
        this.bezirk = bezirk;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getCb() {
        return cb;
    }

    public void setCb(String cb) {
        this.cb = cb;
    }

    public String getEcoli() {
        return ecoli;
    }

    public void setEcoli(String ecoli) {
        this.ecoli = ecoli;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public String getSichttiefe() {
        return sichttiefe;
    }

    public void setSichttiefe(String sichttiefe) {
        this.sichttiefe = sichttiefe;
    }

    public String getTemperatur() {
        return temperatur;
    }

    public void setTemperatur(String temperatur) {
        this.temperatur = temperatur;
    }

    public String getRssName() {
        return rssName;
    }

    public void setRssName(String rssName) {
        this.rssName = rssName;
    }

    public int getWasserquali() {
        return wasserquali;
    }

    public void setWasserquali(int wasserquali) {
        this.wasserquali = wasserquali;
    }
}
