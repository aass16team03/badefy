package com.example.crazy.badefy;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class InfolistActivity extends AppCompatActivity {
    ListView infoListView;
    List<Badestelle> badeListe = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //ProgressDialog.show(this, "Loading", "Wait while loading...");
        super.onCreate(savedInstanceState);
        setTitle("Liste Badestellen Berlin");
        setContentView(R.layout.activity_infolist);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        infoListView = (ListView) findViewById(R.id.infoListView);
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

        if (pruefeInternetverbindung()) {
            new JSONTask().execute("http://www.berlin.de/lageso/gesundheit/gesundheitsschutz/badegewaesser/liste-der-badestellen/index.php/index/all.json?q=");
        } else {
            badeListe = loadDataFromFile();
            if (badeListe != null) {
                listeBadestellen(badeListe);
                // Log.d("Test Array", badeListe.get(0).getBezirk());

            } else {
                Log.d("InfolistActivity:", "Badeliste is empty");
            }
        }

    }

    public void listeBadestellen(List<Badestelle> s) {
        final List<Badestelle> bade = s;
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

        HashMap<String, String> item;
        for (int i = 0; i < s.size(); i++) {
            item = new HashMap<String, String>();
            item.put("line1", s.get(i).getName());
            item.put("line2", s.get(i).getBezirk());
            list.add(item);
        }

        SimpleAdapter adapter = new SimpleAdapter(InfolistActivity.this, list,
                android.R.layout.two_line_list_item,
                new String[]{"line1", "line2"},
                new int[]{android.R.id.text1, android.R.id.text2});

        infoListView.setAdapter(adapter);

        infoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(InfolistActivity.this, InfoTabelleActivity.class);
                intent.putExtra("badename", bade.get(position).getName());
                intent.putExtra("bezirk", bade.get(position).getBezirk());
                intent.putExtra("datum", bade.get(position).getDatum());
                intent.putExtra("cb", bade.get(position).getCb());
                intent.putExtra("ecoli", bade.get(position).getEcoli());
                intent.putExtra("ie", bade.get(position).getIe());
                intent.putExtra("sicht", bade.get(position).getSichttiefe());
                intent.putExtra("temp", bade.get(position).getTemperatur());
                intent.putExtra("quali", bade.get(position).getWasserquali());
                intent.putExtra("rss", bade.get(position).getRssName());
                startActivity(intent);

            }
        });
    }


    public Boolean saveDataIntoFile(JSONArray json) {
        String str = json.toString();
        SharedPreferences sharedPref = getSharedPreferences("appData", Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefEditor = getSharedPreferences("appData", Context.MODE_WORLD_WRITEABLE).edit();
        prefEditor.putString("jsondata", str);
        prefEditor.commit();
        return false;
    }

    public List<Badestelle> loadDataFromFile() {
        SharedPreferences sharedPref = getSharedPreferences("appData", Context.MODE_WORLD_WRITEABLE);
        String strJson = sharedPref.getString("jsondata", "0");
        if (strJson != null) try {
            JSONArray jsonData = new JSONArray(strJson);

            List<Badestelle> listeBadestelle = new ArrayList<>();

            for (int i = 0; i < jsonData.length(); i++) {
                JSONObject finalObject = jsonData.getJSONObject(i);
                Badestelle badestelle = new Badestelle();
                badestelle.setId(finalObject.getInt("id"));
                badestelle.setWasserquali(finalObject.getInt("wasserqualitaet"));
                badestelle.setName(finalObject.getString("badname"));
                badestelle.setDatum(finalObject.getString("dat"));
                badestelle.setBezirk(finalObject.getString("bezirk"));
                badestelle.setCb(finalObject.getString("cb"));
                badestelle.setEcoli(finalObject.getString("eco"));
                badestelle.setIe(finalObject.getString("ente"));
                badestelle.setSichttiefe(finalObject.getString("sicht"));
                badestelle.setTemperatur(finalObject.getString("temp"));
                badestelle.setRssName(finalObject.getString("rss_name"));
                badestelle.setWasserquali(finalObject.getInt("wasserqualitaet"));
                listeBadestelle.add(badestelle);
            }
            return listeBadestelle;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean pruefeInternetverbindung() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInf = cm.getActiveNetworkInfo();
        return  netInf.isConnectedOrConnecting() && netInf != null;

    }

    public class JSONTask extends AsyncTask<String, String, List<Badestelle>> {

        @Override
        protected List<Badestelle> doInBackground(String... params) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();

                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                String finalJson = buffer.toString();
                JSONObject parentObject = new JSONObject(finalJson);
                JSONArray arrayObject = parentObject.getJSONArray("index");

                saveDataIntoFile(arrayObject);

                badeListe = new ArrayList<>();

                for (int i = 0; i < arrayObject.length(); i++) {
                    JSONObject finalObject = arrayObject.getJSONObject(i);
                    Badestelle badestelle = new Badestelle();
                    badestelle.setId(finalObject.getInt("id"));
                    badestelle.setWasserquali(finalObject.getInt("wasserqualitaet"));
                    badestelle.setName(finalObject.getString("badname"));
                    badestelle.setDatum(finalObject.getString("dat"));
                    badestelle.setBezirk(finalObject.getString("bezirk"));
                    badestelle.setCb(finalObject.getString("cb"));
                    badestelle.setEcoli(finalObject.getString("eco"));
                    badestelle.setIe(finalObject.getString("ente"));
                    badestelle.setSichttiefe(finalObject.getString("sicht"));
                    badestelle.setTemperatur(finalObject.getString("temp"));
                    badestelle.setRssName(finalObject.getString("rss_name"));
                    badestelle.setWasserquali(finalObject.getInt("wasserqualitaet"));
                    badeListe.add(badestelle);
                }
                return badeListe;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (connection != null) {
                    connection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Badestelle> s) {
            super.onPostExecute(s);
            listeBadestellen(s);
        }
    }

}
